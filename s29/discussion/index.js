db.users.find({Age: {$gt: 50}}).pretty();
db.users.find({Age: {$gte: 50}}).pretty();

//lly $lt, $lte
//lly $ne -- not equal


// $in -- specific math criteria exact
db.users.find({lastName: {$in: ["Hawking","Doe"]}});

//$or -- match a single criteria from multiple
db.users.find({$or: [{firstName: "Neil"},{age: 25}]});

//lly $and


//want specific data from the user
//keep value to be 1 if include else 0 to exclude.
db.users.find({firstName: "Jane"},{firstName: 1,lastName: 1,contact: 1});

//$slice operator
//$regex to find that match specific string pattern

