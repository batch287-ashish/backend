

let username;
let password;
let role;



username = prompt("Enter your username: ");
password = prompt("Enter your password: ");
role = prompt("Enter your role: ");

if((username===""||username===null)||(password===""||password==null)||(role===""||role==null)){
    alert("Input should not be empty");
}else{
    switch(role){
        case 'admin':
            alert("Welcome back to the class portal, admin!");
            break;
        case 'teacher':
            alert("Thank you for logging in, teacher!");
            break;
        case 'rookie':
            alert("Welcome to the class portal, student!");
            break;
        default:
            alert("Role out of range.");
            break;
    }
}



if(role==="teacher"||role===null||role===undefined||role==="admin"){
    alert(role+"! You are not allowed to access this feature!.");
}else{

    function checkAverage(num1,num2,num3,num4){
        let average = (num1+num2+num3+num4)/4;
        average = Math.round(average);
        
        if(average<=74){
            console.log("Hello, student, your average is: "+average+". The letter equivalent is F");
        }else if(average>=75&&average<=79){
            console.log("Hello, student, your average is: "+average+". The letter equivalent is D");
        }else if(average>=80&&average<=84){
            console.log("Hello, student, your average is: "+average+". The letter equivalent is C");
        }else if(average>=85&&average<=89){
            console.log("Hello, student, your average is: "+average+". The letter equivalent is B");
        }else if(average>=90&&average<=95){
            console.log("Hello, student, your average is: "+average+". The letter equivalent is A");
        }else{
            console.log("Hello, student, your average is: "+average+". The letter equivalent is A+");
        }
    }

    checkAverage(71,70,73,74);
    checkAverage(75,75,76,78);
    checkAverage(80,81,82,78);
    checkAverage(84,85,87,88);
    checkAverage(89,90,91,90);
    checkAverage(91,96,97,95);
    checkAverage(91,96,97,99);
}
