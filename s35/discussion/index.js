const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.brmhau3.mongodb.net/s35-discussion",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("WE WERE CONNECTED TO THE CLOUD DATABASE!!"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task",taskSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/tasks",(req,res)=>{
	Task.findOne({name: req.body.name}).then((result,err)=>{
		if(result != null && result.name==req.body.name){
			return res.send("Duplicate task found...");
		}
		else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save().then((savedTask,saveErr)=>{
				if(saveErr) return console.log(saveErr);
				else return res.status(201).send("NEW TASK CREATED...");
			})
		}
	})
})

app.get("/tasks",(req,res)=>{
	Task.find({}).then((result,err)=>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port,()=>console.log("server is running"));



