const express = require("express");
const mongoose = require("mongoose");

const port = 4001;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.brmhau3.mongodb.net/s35-activity",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("WE WERE CONNECTED TO THE CLOUD DATABASE!!"));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User",userSchema);

app.post("/signup",(req,res)=>{
	User.findOne({"username": req.body.username}).then((result,err)=>{
		if(result!=null&&result.username===req.body.username){
			return res.send("Username already exists...");
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save().then((savedUser,saveErr)=>{
				if(saveErr) return console.log(saveErr);
				else return res.status(201).send("NEW USER REGISTERED...");
			})
		}
	})
})

app.listen(port,()=>console.log("server is running"));