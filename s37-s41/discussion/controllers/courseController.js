const Course = require("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (data) => {

	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course,err) => {
			if(err) return false;
			else return true;
		});
	}

	let message = Promise.resolve("User must be an Admin to access this..");

	return message.then((val)=>{
		return val;
	});
	
};

module.exports.getAllCourses = () => {
	return  Course.find({}).then(result => {
		return result;
	});
};

module.exports.activeCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams,reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,err) => {
		if(err) return false;
		else return true;
	});
};

module.exports.archiveCourse = (reqParams) => {
	// if(data.isAdmin){
	// 	let updatedCourse = {
	// 		isActive: data.course.isActive
	// 	};
	// 	return Course.findByIdAndUpdate(data.courseId,updatedCourse).then((course,err) => {
	// 		if(err) return false;
	// 		return  true;
	// 	});
	// }

	let updateActiveField={
		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId,updateActiveField).then((course,error) => {
		if(error) return false;
		else return true;
	});

	// let msg = Promise.resolve("User must be an Admin to access this..");

	// return msg.then((val) => {
	// 	return val;
	// });
};