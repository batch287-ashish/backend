const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js")
const courseRoutes = require("./routes/courseRoute.js")
//CROSS-ORIGIN RESOURCE SHARING
const cors = require("cors");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.brmhau3.mongodb.net/s37-s41",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open",() => console.log("NOW WE WERE CONNECTED TO THE CLOUD..."));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users",userRoutes);
app.use("/courses",courseRoutes);

app.listen(process.env.PORT||4000,()=>{
	console.log(`API is now online on port ${process.env.PORT||4000}`);
});

