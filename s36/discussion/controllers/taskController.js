const Task = require("../models/task");

module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=>{
		return result;
	});
};

module.exports.createTask = (reqBody)=>{
	let newTask = new Task({
		name: reqBody.name,
	})
	return newTask.save().then((task,err)=>{
		if(err) console.log(err);
		else return task;
	})
}

module.exports.deleteTask = (id)=>{
	return Task.findByIdAndRemove(id).then((removedTask,err)=>{
		if(err) console.log(err);
		else return "Deleted Task...";
	})
}

module.exports.updateTask = (id,reqBody)=>{
	return Task.findById(id).then((updateTask,err)=>{
		if(err) console.log(err);
		updateTask.name=reqBody.name;
		return updateTask.save().then((updatedTask,saveErr)=>{
			if(saveErr) console.log(saveErr);
			else return "Task updated...";
		})
	})
}