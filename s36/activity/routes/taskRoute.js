const express = require("express");
const taskController = require("../controllers/taskController");
const router = express.Router();

router.get("/",(req,res)=>{
	taskController.getAllTasks().then(result => res.send(result));
});

router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(result => res.send(result));
})

router.get("/:id",(req,res)=>{
	taskController.getSpecificTask(req.params.id).then(result => res.send(result));
});

router.put("/:id/complete",(req,res)=>{
	taskController.changeStatus(req.params.id).then(result => res.send(result));
});

module.exports = router;