const Task = require("../models/task");

module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=>{
		return result;
	});
};

module.exports.createTask = (reqBody)=>{
	let newTask = new Task({
		name: reqBody.name,
	})
	return newTask.save().then((task,err)=>{
		if(err) console.log(err);
		else return task;
	})
}

module.exports.getSpecificTask = (id)=>{
	return Task.findById(id).then(result=>{
		return result;
	})
}

module.exports.changeStatus = (id,reqBody)=>{
	return Task.findById(id).then((updateStatus,err)=>{
		if(err) console.log(err);
		updateStatus.status="complete";
		return updateStatus.save().then((updatedStatus,saveErr)=>{
			if(saveErr) console.log(saveErr);
			return updatedStatus;
		})
	})
}