const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

const port = 4001;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.brmhau3.mongodb.net/s36-discussion",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open",()=>console.log("WE WERE CONNECTED TO THE CLOUD DATABASE..."));

app.listen(port,() => console.log(`currently listening to port ${port}`));

app.use("/tasks",taskRoute);