const express = require("express");
const port = 4000;
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.listen(port,() => console.log("server is running"));

app.get("/home",(req,res)=>{
	res.send("Welcome to the home page");
});

let users=[];
app.post("/add-user",(req,res)=>{
	if(req.username!==""||req.password!==""){
		users.push(req.body);
		res.send("user added successfully");
	}
	else res.send("username and password should not be empty");
});

app.get("/users",(req,res)=>{
	res.send(users);
});

app.delete("/delete-user",(req,res)=>{
	let flag=false;
	for(let i=0;i<users.length;i++){
		if(req.body.username===users[i].username){
			flag=true;
			users.splice(i,1);
			return res.send(`User ${req.body.username} has been deleted`);
		}
	}
	if(flag==false) return res.send("User not found!!");
})




