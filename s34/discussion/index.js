const express = require("express");

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(port,() => console.log(`Server is running at port ${port}`));

app.get("/hello",(req,res) => {
	res.send('Hello fron the /hello');
});

app.post("/display-name",(req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

