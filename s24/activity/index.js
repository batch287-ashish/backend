let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

const address = ["258 Washington Ave NW","California",90011];
displayAddress = ([street,state,code])=>{
	console.log(`I live at ${street}, ${state} ${code}`);
}
displayAddress(address);

let animal = {
	name: "Lolong",
	type: "saltwater",
	weight: 1075,
	measurment: "20 ft 3 in",
}
const {name,type,weight,measurment} = animal;
console.log(`${name} was a ${type} crocodile. He weighed at ${weight} kgs with a measurment of ${measurment}.`);

let numbers = [1,2,3,4,5];
numbers.forEach((num)=>{
	console.log(num);
})
let reduceNumber = numbers.reduce((x,y) => x+y);
console.log(reduceNumber);

class Dog{
	constructor(name,age,breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let myDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(myDog);
