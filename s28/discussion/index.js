db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});
db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    Age: 21,
    contact: {
        phone: "1234567890",
        email: "janedoe@mail.com"
    },
    courses: ["CSS","JS","Python"],
    department: "none"
});
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        Age: 76,
        contact: {
            phone: "1234567890",
            email: "janedoe@mail.com"
        },
        courses: ["CSS","React","Python"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        Age: 82,
        contact: {
            phone: "1234567890",
            email: "janedoe@mail.com"
        },
        courses: ["Bootstrap","React","Python"],
        department: "none"
    },
]);

db.users.find();
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "1234567890",
        email: "test@mail.com"
    },
    courses: [],
    department: "none"
});
db.users.updateOne(
    {firstName: "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName: "gates",
            age: 0,
            contact: {
                phone: "1234567890",
                email: "test@mail.com"
            },
            courses: ["PHP","SQL","HTML"],
            department: "none"
        }
    }
);

db.users.deleteOne({firstName: "Bill1"});