let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);


function addUser(newUser){
    users.push(newUser);
}

addUser("John cena");
console.log(users);




let itemFound;

function getElement(ind){
    return users[ind];
}

itemFound = getElement(2);

console.log(itemFound);






let lastItem;

function deleteLastElement(){
    let lastElement;
    lastElement = users[users.length-1];
    users.length--;
    return lastElement;
}

lastItem = deleteLastElement();
console.log(lastItem);

console.log(users);





function update(name,index){
    users[index] = name;
}

update("Triple H",3);
console.log(users);




function deleteAllElements(users){
    while(users.length>0) users.length--;
}
deleteAllElements(users);
console.log(users);





let isUsersEmpty;

function checkEmpty(users){
    return (users.length===0)?true:false;
}

isUsersEmpty = checkEmpty(users);
console.log(isUsersEmpty);