fetch("https://jsonplaceholder.typicode.com/posts").then((res) => console.log(res.status));

fetch("https://jsonplaceholder.typicode.com/posts").then((res)=>res.json()).then((data)=>console.log(data));

async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	let json=await result.json();
	console.log(json);
}
fetchData();

fetch("https://jsonplaceholder.typicode.com/posts/1").then((res)=>res.json()).then((data)=>console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts",{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "New Post",
		body: "hello",
		userId: 1,
	})
}).then((res)=>res.json()).then((data)=>console.log(data));




