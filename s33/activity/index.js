fetch("https://jsonplaceholder.typicode.com/todos",{
	method: 'GET'
}).then((res)=>res.json()).then((data)=>{
	let newData = data.map(({userId,id,title,completed})=>{
		return title;
	})
	console.log(newData);
});

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET'
}).then((res)=>res.json()).then((data)=>console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET'
}).then((res)=>res.json()).then((data)=>{
	console.log(`The item "${data.title}" on the list has a status of ${data.completed}`);
});

fetch("https://jsonplaceholder.typicode.com/todos",{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1,
	})
}).then((res)=>res.json()).then((data)=>console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		dateCompleted: "pending",
		status: "pending",
		description: "To update the my to do list with a different data structures",
		userId: 1,
	})
}).then((res)=>res.json()).then((data)=>console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
}).then((res)=>res.json()).then((data)=>console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'DELETE'
});