//JSON -- javascript object notation.

//JSON arrays
/*
"cities":[
	{
		"PA":"va",
		"PB":"vb"
	},
	{
		"PA":"va",
		"PB":"vb"
	},
	{
		"PA":"va",
		"PB":"vb"
	}
]
*/

//JSON object contains methods for 
//parsing and converting data into stringified JSON..

let batchesArr = [{batchName:"batch X"},{batchName:"batch X"}];
console.log(typeof(batchesArr));
console.log(JSON.stringify(batchesArr));

//Stringified JSON to JS object

//JSON.parse();