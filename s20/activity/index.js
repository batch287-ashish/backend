let number = prompt("Enter a number: ");

console.log("The number you provided is "+number+".");

for(number;number>=0;number--){
	if(number<=50){
		console.log("The current value is at 50. Terminating the loop");
		break;
	}
	if(number%10===0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if(number%5===0) console.log(number);
}

let s1 = "supercalifragilisticexpialidocious";
let s2 = "";

console.log(s1);

for(let i=0;i<s1.length;i++){
	if(s1[i]==='a'||s1[i]==='e'||s1[i]==='i'||s1[i]==='o'||s1[i]==='u') continue;
	else s2+=s1[i];
}

console.log(s2);
