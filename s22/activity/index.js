let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];



function register(user){
    if(registeredUsers.includes(user)) return alert("Registration failed. Username already exists!");
    else{
        alert("Thank you for registering!");
        registeredUsers.push(user);
    }
}
    



function addFriend(user){
    if(registeredUsers.includes(user)){
        alert("You have added " +user+" as a friend!");
        friendsList.push(user);
    }
    else{
        alert("User not found.");
    }
}




function displayFriends(){
    if(friendsList.length===0) alert("You currently have 0 friends. Add one first.");
    else{
        friendsList.forEach(function(friend){
            console.log(friend);
        })
    }
}
    



function displayNumberOfFriends(){
    if(friendsList.length===0) alert("You currently have 0 friends. Add one first.");
    else{
        alert("You currently have "+ friendsList.length +" friends.")
    }
}


function deleteFriend(){
    if(friendsList.length===0) alert("You currently have 0 friends. Add one first.");
    else{
        friendsList.pop();
    }
}


function deleteSpecificFriend(friend){
    if(friendsList.length===0) alert("You currently have 0 friends. Add one first.");
    else{
        let index = friendsList.indexOf(friend);
        friendsList.splice(index,1);
    }
}
