let http = require("http");

let directory=[
	{
		"name": "Brandon",
		"email": "brandon@email.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@email.com"
	}
]
http.createServer((req,res)=>{
	if(req.url=='/users'&&req.method=='GET'){
		res.writeHead(200,{'content-type':'application/json'});
		res.write(JSON.stringify(directory));
		res.end();
	}
	if(req.url=='/users'&&req.method=='POST'){
		let reqBody='';
		req.on('data',(data)=>{
			reqBody+=data;
		})
		req.on('end',()=>{
			reqBody=JSON.parse(reqBody);
			let newUser={
				"name": reqBody.name,
				"email": reqBody.email
			};
			directory.push(newUser);
			res.writeHead(200,{'content-type':'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
}).listen(4001);
console.log("Server is running at localhost:4001");