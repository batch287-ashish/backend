let http = require("http");
http.createServer((req,res)=>{
	if(req.url=='/items'&&req.method=='GET'){
		res.writeHead(200,{'content-type':'text/plain'});
		res.end('Data retrieved from the database');
	}
	if(req.url=='/items'&&req.method=='POST'){
		res.writeHead(200,{'content-type':'text/plain'});
		res.end('Data to be sent to the database');
	}
}).listen(4000);
console.log('Sever is running at localhost:4000');