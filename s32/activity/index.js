let http = require("http");
http.createServer((req,res)=>{
	if(req.url=="/"&&req.method=='GET'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Welcome to the Booking System");
	}
	else if(req.url=="/profile"&&req.method=='GET'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Welcome to your profile");
	}
	else if(req.url=="/courses"&&req.method=='GET'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Here's our courses available");
	}
	else if(req.url=="/addCourse"&&req.method=='POST'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Add course to our resources");
	}
	else if(req.url=="/updateCourse"&&req.method=='PUT'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Update a course to our resources");
	}
	else if(req.url=="/archiveCourse"&&req.method=='DELETE'){
		res.writeHead(200,{'content-type':"application/json"});
		res.end("Archive courses to our resources");
	}

}).listen(4000);
console.log("Server running");